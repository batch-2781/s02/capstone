# [SECTION] Models
# Each model is represented by a class that inherits the django.db.models.Model. Each model has a number of class variables, each of which represents a database field in the model
# Each field is represented by an instance of a field class e.g. Charfield for charFields and DateTimeField for date times. this tells django what type of data each field holds.
# Some field classes have required arguments. Charfield requires that you give it a max_length
# A field can also have various optional


# [SECTION] Migration
python manage.py makemigrations todolist
# By running makemigrations, you,re telling django that you've made some changes to your models

# migrate
python manage.py sqlmigrate todolist 0001
python manage.py migrate

# to run the python shell
python manage.py shell

todoitem = ToDoItem(task_name = "EAT", description = "Dinner", date_created = timezone.now())

groceryitem = GroceryItem(item_name = "Lays", category = "Food", date_created = timezone.now())

#save
todoitem.save()

# for mysql connection
pip install mysql_connector
pip install mysql

# or
pip install mysql_connector mysql


# creating superuser
#macOS/linux
python3 manage.py createsuperuser

# windows
winpty python manage.py createsuperuser

