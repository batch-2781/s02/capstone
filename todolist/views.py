from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.


#The from keyword allows importing of necessary classes/modules, methods and other files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

#Local imports
from .models import ToDoItem, EventItem

from django.contrib.auth.models import User

#to use the template created:
from django.template import loader

from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegistrationForm, AddEventForm, UpdateEventForm

from django.utils import timezone

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	eventitem_list = EventItem.objects.filter(user_id = request.user.id)
	template = loader.get_template("index.html")
	context = {
		'todoitem_list': todoitem_list,
		'eventitem_list': eventitem_list,
		"user": request.user
	}
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
	return HttpResponse(template.render(context, request))
	# return HttpResponse("Hello from the views.py file!")

def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))

# def register(request):

	# if request.method == 'POST':

	# 	form = RegistrationForm(request.POST)

	# 	if form.is_valid():
	# 		form.save()
	# 		login(request, user)
	# 		messages.success(request, "Registration successful." )
	# 		return redirect('todolist:login')
	# 	messages.error(request, "Unsuccessful registration. Invalid information.")
	# else: 
	# 	form = RegistrationForm()
	# return render(request=request, template_name='register.html', context={ 'form': form })

def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']

            if password1 == password2:
                # check if the user already exists in the database
                if User.objects.filter(username=username).exists():
                    form.add_error('username', 'This username is already taken.')
                else:
                    user = User.objects.create_user(username=username, first_name=first_name, last_name=last_name, email=email, password=password1)
                    user.is_staff = False
                    user.is_active = True
                    user.save()

                    context = {
                        'is_user_registered': True,
                        'first_name': first_name,
                        'last_name': last_name
                    }
                    return render(request, "login.html", context)
            else:
                form.add_error('password2', 'Passwords do not match.')
    else:
        form = RegistrationForm()

    context = {
        'form': form,
    }
    return render(request, 'register.html', context)

def change_password(request):

	is_user_authenticated = False

	user = authenticate(username= "johndoe", password = "john1234")

	if user is not None:
		authenticated_user = User.objects.get(username = 'johndoe')
		authenticated_user.set_password("johndoe12345")
		authenticated_user.save()

		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "change_password.html", context)


def login_user(request):
	context={}

	if request.method == "POST":

		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()
		else :

			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password =  password)

			if user is not None:
				context = {
					'username': username,
					'password': password
				}
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
			
	return render(request, "login.html", context)


	# username = 'johndoe'
	# password = 'johndoe12345'

	# is_user_authenticated = False

	# user = authenticate(username = username, password = password)

	

def logout_user(request):
	logout(request)
	return redirect("todolist:index")


def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:

				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")

			else:
				context = {
					'error': True
				}
	
	return render(request, "add_task.html", context)

def update_task(request,todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id)

	context = {
		'user': request.user,
		'todoitem_id': todoitem_id,
		'task_name': todoitem[0].task_name,
		'description': todoitem[0].description,
		'status': todoitem[0].status
	}

	if request.method == "POST":
		form = UpdateTaskForm(request.POST)

		if form.is_valid == False:
			form =  UpdateTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:

				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()

				return redirect("todolist:viewtodoitem", todoitem_id =  todoitem[0].id)

			else:

				context = {
					'error': True
				}

	return render(request, "update_task.html, context")

def delete_task(request, todoitem_id):

	ToDoItem.objects.filter(pk = todoitem_id).delete()

	return redirect("todolist:index")

def eventitem(request, eventitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	eventitem = get_object_or_404(EventItem, pk = eventitem_id)

	return render(request, "eventitem.html", model_to_dict(eventitem))

def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()
		else:
			name = form.cleaned_data['name']
			description = form.cleaned_data['description']

			duplicates = EventItem.objects.filter(name = name, user_id = request.user.id)

			if not duplicates:

				EventItem.objects.create(name = name, description = description, event_date = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")

			else:
				context = {
					'error': True
				}
	
	return render(request, "add_event.html", context)

def update_event(request,eventitem_id):

	eventitem = EventItem.objects.filter(pk = eventitem_id)

	context = {
		'user': request.user,
		'eventitem_id': eventitem_id,
		'name': eventitem[0].name,
		'description': eventitem[0].description,
		'status': eventitem[0].status
	}

	if request.method == "POST":
		form = UpdateEventForm(request.POST)

		if form.is_valid == False:
			form =  UpdateEventForm()

		else:
			name = form.cleaned_data['name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:

				eventitem[0].name = name
				eventitem[0].description = description
				eventitem[0].status = status

				eventitem[0].save()

				return redirect("todolist:vieweventitem", eventitem_id =  eventitem[0].id)

			else:

				context = {
					'error': True
				}

	return render(request, "update_event.html, context")

def delete_event(request, eventitem_id):

	EventItem.objects.filter(pk = eventitem_id).delete()

	return redirect("todolist:index")

